//
//  WebViewController.h
//  Steampunk
//
//  Created by Ariel Elkin on 09/07/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController <UIWebViewDelegate>{
    IBOutlet UIWebView *webView;
    IBOutlet UIActivityIndicatorView *activityIndicator;
}

-(IBAction)closeWebView;
-(IBAction)showFacebook;
-(IBAction)showTwitter;
-(IBAction)showWebsite;

@end
