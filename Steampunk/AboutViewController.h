//
//  AboutViewController.h
//  Steampunk
//
//  Created by Ariel Elkin on 12/07/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AboutViewController : UIViewController{
    IBOutlet UITextView *textView;
}

-(IBAction)closeAboutView;


@end
