//
//  SplashViewController.h
//  Steampunk
//
//  Created by Ariel on 19/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface SplashViewController : UIViewController{
    IBOutlet UIImageView *background;
    IBOutlet UIImageView *logo;
    UIImageView *instructions;
}

@property (nonatomic) IBOutlet UIImageView *logo;
@property (nonatomic) IBOutlet UIImageView *background;
@property (nonatomic) UIImageView *instructions;

@end
