//
//  ViewController.m
//  Steampunk
//
//  Created by Ariel on 14/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//


#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "mo_audio.h"

#define SRATE 22050
//default: 44100, low:32000, lower: 22050
#define FRAMESIZE 128 
//default:128
#define NUMCHANNELS 2

bool topValveOn = false;
bool middleValveOn = false;
bool bottomValveOn = false;

bool audio_on = false;

float amp = 0;

AVAudioPlayer *sound_ValvePressed;
AVAudioPlayer *sound_On;
AVAudioPlayer *sound_Off;

NSUserDefaults *userDefaults;

#ifdef LITEVERSION
NSTimer *adTimer;
int adViewInterval = 45;
#endif

#pragma mark -
#pragma mark Audio Callback
void audioCallback( Float32 * buffer, UInt32 framesize, void* userData)
{
	AudioData * data = (AudioData*) userData;
    

    for(int i=0; i<framesize; i++)
    {
        SAMPLE in = buffer[2*i];
        if(audio_on){
            
            if(topValveOn){
                in = data->firstPole->tick(in);
                in = data->chorus1->tick(in);
                in = data->reverb1->tick(in);
            }
            
            if (middleValveOn){
    //            in = in + data->echo1->tick(data->reverb1->tick(in));
                //data->sine1->setFrequency(in * 1000);
    //            data->sine1->setFrequency(in * 10000);
                data->sine1->setFrequency(data->myAccel->getY() * 900);
                in = in + data->sine1->tick() * 0.5;
                in = data->reverb1->tick(data->echo1->tick(in));

            }

            if(bottomValveOn){
                in = in * data->sine1->tick();
                in = data->firstPole->tick(in);
                in = data->chorus1->tick(in);
            }
            
            SAMPLE out = in;
            buffer[2*i] = buffer[2*i+1] = out;
            amp = out;
        }
        else {
            buffer[2*i] = buffer[2*i+1] = 0;
        }
    }
}

@implementation ViewController
@synthesize topValve, middleValve, bottomValve;
@synthesize topFuse, bottomFuse;
@synthesize glower, slider;
@synthesize greenGem;
@synthesize glowTimer, onOffSwitch;
@synthesize settingsButton;
@synthesize tutorialView;

#pragma mark -
#pragma mark Valves controls

-(IBAction)topValvePressed{
    if(topValveOn){ 
        topValveOn = false;
        [topValve setImage:[UIImage imageNamed:@"top_valve_inactive.png"] forState:UIControlStateNormal];
    }
    else {
        [sound_ValvePressed play];
        audioData.firstPole->setPole(0.1);
        audioData.firstPole->setGain(3);
//        audioData.secondPole->setPole(0.9);3
//        audioData.secondPole->setGain(5);
        audioData.reverb1->setT60(1);
        audioData.reverb1->setEffectMix(0.5);
        audioData.chorus1->setModDepth(1);
        audioData.chorus1->setModFrequency(100);
        audioData.chorus1->setEffectMix(0.8);
        
        topValveOn = true;
        [topValve setImage:[UIImage imageNamed:@"top_valve_pressed.png"] forState:UIControlStateNormal];
        
        middleValveOn = false;
        [middleValve setImage:[UIImage imageNamed:@"middle_valve_inactive.png"] forState:UIControlStateNormal];
        bottomValveOn = false;
        [bottomValve setImage:[UIImage imageNamed:@"bottom_valve_inactive.png"] forState:UIControlStateNormal];

    }
}

-(IBAction)middleValvePressed{
    if(middleValveOn){
        middleValveOn = false;
        [middleValve setImage:[UIImage imageNamed:@"middle_valve_inactive.png"] forState:UIControlStateNormal];
    }
    else {
        [sound_ValvePressed play];
        
        audioData.sine1->setFrequency(400);
        audioData.reverb1->setEffectMix(0.5);

        middleValveOn = true;
        [middleValve setImage:[UIImage imageNamed:@"middle_valve_pressed.png"] forState:UIControlStateNormal];
        
        topValveOn = false;
        [topValve setImage:[UIImage imageNamed:@"top_valve_inactive.png"] forState:UIControlStateNormal];
        bottomValveOn = false;
        [bottomValve setImage:[UIImage imageNamed:@"bottom_valve_inactive.png"] forState:UIControlStateNormal];
    }
}


-(IBAction)bottomValvePressed{
    if(bottomValveOn){
        bottomValveOn = false;
        [bottomValve setImage:[UIImage imageNamed:@"bottom_valve_inactive.png"] forState:UIControlStateNormal];
    }
    else { 
        [sound_ValvePressed play];
        audioData.chorus1->setModDepth(0.17);
        audioData.chorus1->setModFrequency(2800);
        audioData.chorus1->setEffectMix(1);
        
        audioData.firstPole->setGain(9.9);
        audioData.firstPole->setPole(0.6);
        audioData.sine1->setFrequency(20);
        
        bottomValveOn = true;
        [bottomValve setImage:[UIImage imageNamed:@"bottom_valve_pressed.png"] forState:UIControlStateNormal];
        
        topValveOn = false;
        [topValve setImage:[UIImage imageNamed:@"top_valve_inactive.png"] forState:UIControlStateNormal];
        middleValveOn = false;
        [middleValve setImage:[UIImage imageNamed:@"middle_valve_inactive.png"] forState:UIControlStateNormal];
    }
}


#pragma mark -
#pragma mark Gear slider

-(void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [[event allTouches] anyObject];
    CGPoint touchLocation = [touch locationInView:self.view];
    
    if([touch view] == slider && touchLocation.y > 308 && touchLocation.y < 400){
        float pos = (slider.center.y - 300 )/100;
        NSLog(@"slider is at %f", pos);
        
        slider.center = CGPointMake(slider.center.x, touchLocation.y);
        audioData.chorus1->setModFrequency(touchLocation.y*8);
        if(bottomValveOn){
            audioData.sine1->setFrequency(pos * 10 );
        }
        else if(middleValveOn){
            audioData.echo1->setDelay(pos * 100000);
        }
        else {
            audioData.sine1->setFrequency(pos * 10000);
        }
        audioData.reverb1->setT60(pos);
    }
}


#pragma mark -
#pragma mark Toggle Audio

-(IBAction)toggleAudio{
    if(audio_on){
        [sound_Off play];
        [UIView animateWithDuration:1.9
                              delay:nil
         options:UIViewAnimationOptionCurveEaseOut
                         animations:^{onOffSwitch.transform = CGAffineTransformMakeRotation(1.77);}
                          completion:nil
         ];
        audio_on = false;
        greenGem.image = [UIImage imageNamed:@"looper_fuse_inactive.png"];
        topValve.enabled = NO;
        middleValve.enabled = NO;
        bottomValve.enabled = NO;

    } else if(!audio_on && !sound_Off.playing) {
        [sound_On play];
        [UIView animateWithDuration:0.55
                              delay:nil
                            options:UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             onOffSwitch.transform = CGAffineTransformMakeRotation(0);
                         }
                         completion:nil
         ];
        audio_on = true;   
        greenGem.image = [UIImage imageNamed:@"looper_fuse_active.png"];
        topValve.enabled = YES;
        middleValve.enabled = YES;
        bottomValve.enabled = YES;
    }
}

-(void)glow{
    glower.alpha = fabs(amp);
}

-(void) initInstruments{
    audioData.chorus1 = new Chorus(50);
    audioData.firstPole = new MoOnePole();
    audioData.secondPole = new MoOnePole();
    audioData.sine1 = new SineWave();
    audioData.sine1->setFrequency(400);
    
    audioData.echo1 = new Echo();
    audioData.reverb1 = new JCRev();
    audioData.brass = new Brass(400);
}


#pragma mark -
#pragma mark viewDidLoad

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    
    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background.png"]];
    background.center = CGPointMake(160, 240);
    [self.view addSubview:background];
    [self.view sendSubviewToBack:background];
    
    [self initInstruments];
    
	// init audio
	NSLog(@"Initializing Audio");
	
	// init the MoAudio layer
	bool result = MoAudio::init(SRATE, FRAMESIZE, NUMCHANNELS);
	
	if (!result)
	{
		NSLog(@"cannot initialize real-time audio!");
		return;
	}
	
	// start the audio layer, registering a callback method
	result = MoAudio::start( audioCallback, &audioData);
	if (!result)
	{
		NSLog(@"cannot start real-time audio!");
		return;
	}
    
    glowTimer = [NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(glow) userInfo:nil repeats:YES];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    
	if(![userDefaults integerForKey:@"timesAppRan"]){
		[userDefaults setInteger:1 forKey:@"timesAppRan"];
        [userDefaults synchronize];
        
	} else if([userDefaults integerForKey:@"timesAppRan"] < 2){
        [userDefaults setInteger:([userDefaults integerForKey:@"timesAppRan"] + 1) forKey:@"timesAppRan"];
        [userDefaults synchronize];
        
    } else {
        [userDefaults setInteger:([userDefaults integerForKey:@"timesAppRan"] + 1) forKey:@"timesAppRan"];
        [userDefaults synchronize];
        
        #ifdef LITEVERSION
        NSLog(@"Ad Timer: START");
        adTimer = [NSTimer scheduledTimerWithTimeInterval:adViewInterval target:self selector:@selector(showAdView) userInfo:nil repeats:YES];
        #endif
        
    }
    
    NSLog(@"times app ran: %d", [userDefaults integerForKey:@"timesAppRan"]);
    
    onOffSwitch.layer.anchorPoint = CGPointMake(0.25, 0.52);
    onOffSwitch.layer.position = CGPointMake(112, 298);
    onOffSwitch.transform = CGAffineTransformMakeRotation(1.77);
    
	NSError *error = nil;

    sound_ValvePressed = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:
                                                                       [NSString stringWithFormat:@"%@/valvepress.caf", [[NSBundle mainBundle] resourcePath]]] error:&error];
    sound_On = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:
                                                             [NSString stringWithFormat:@"%@/audioSteam_on.caf", [[NSBundle mainBundle] resourcePath]]] error:&error];
    sound_Off = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:
                                                              [NSString stringWithFormat:@"%@/audioSteam_off.caf", [[NSBundle mainBundle] resourcePath]]] error:&error];
    
    if([userDefaults integerForKey:@"timesAppRan"] < 4){
        [self displayStoryWithTutorial:YES];
    }
    
}

#pragma mark -
#pragma mark Tutorial

-(void)displayTutorial{
    #ifdef LITEVERSION    
    NSLog(@"Ad Timer: INVALIDATE");
    [adTimer invalidate];
    #endif
    
    tutorialView = [self.storyboard instantiateViewControllerWithIdentifier:@"Tuts"];
    tutorialView.screenNumber = 1;
    tutorialView.tutorialViewDelegate = self;
    [self.view addSubview:tutorialView.view];
    [UIView animateWithDuration:1
                     animations:^{tutorialView.view.alpha = 1;}
     ];
        
}

-(void)displayStoryWithTutorial:(BOOL)showTutorial{
    #ifdef LITEVERSION    
    NSLog(@"Ad Timer: INVALIDATE");
    [adTimer invalidate];
    #endif
    
    if(!showTutorial){
        tutorialView = [self.storyboard instantiateViewControllerWithIdentifier:@"StoryNoTuts"];
        tutorialView.tutorialViewDelegate = self;
        [self.view addSubview:tutorialView.view];
        [UIView animateWithDuration:1
                         animations:^{tutorialView.view.alpha = 1;}
         ];
    }
    
    else {
        tutorialView = [self.storyboard instantiateViewControllerWithIdentifier:@"StoryTuts"];
        tutorialView.screenNumber = 0;
        tutorialView.tutorialViewDelegate = self;
        [self.view addSubview:tutorialView.view];
        [UIView animateWithDuration:1
                         animations:^{tutorialView.view.alpha = 1;}
         ];
    }
}

-(void)tutorialViewClosed{
    
#ifdef LITEVERSION
    NSLog(@"Ad Timer: START");
    adTimer = [NSTimer scheduledTimerWithTimeInterval:adViewInterval target:self selector:@selector(showAdView) userInfo:nil repeats:NO];
#endif
    
    if(![userDefaults integerForKey:@"timesAppRan"] || [userDefaults integerForKey:@"timesAppRan"] < 3){
        UIImageView *pointer = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/pointer.png", [[NSBundle mainBundle] resourcePath]]]];
        pointer.center = CGPointMake(210, 240);
        [self.view addSubview:pointer];
        
        [UIView animateWithDuration:1
                         animations:^{
                             pointer.center = CGPointMake(195, 260);
                         }
                         completion:^(BOOL finished){
                             [UIView animateWithDuration:0.8
                                              animations:^{
                                                  pointer.center = CGPointMake(210, 240);
                                              }
                                              completion:^(BOOL finished){
                                                  [UIView animateWithDuration:0.8
                                                                   animations:^{
                                                                       pointer.center = CGPointMake(195, 260);
                                                                   }
                                                                   completion:^(BOOL finished){
                                                                       [UIView animateWithDuration:0.8
                                                                                        animations:^{
                                                                                            pointer.center = CGPointMake(210, 240);
                                                                                        }
                                                                                        completion:^(BOOL finished){
                                                                                            [UIView animateWithDuration:0.8
                                                                                                             animations:^{
                                                                                                                 pointer.center = CGPointMake(195, 260);
                                                                                                             }
                                                                                                             completion:^(BOOL finished){
                                                                                                                 [UIView animateWithDuration:1
                                                                                                                                  animations:^{
                                                                                                                                      pointer.alpha = 0;
                                                                                                                                  }
                                                                                                                                  completion:^(BOOL finished){
                                                                                                                                      if(finished) [pointer removeFromSuperview];
                                                                                                                                  }
                                                                                                                  ];
                                                                                                             }
                                                                                             ];
                                                                                            
                                                                                            
                                                                                        }
                                                                        ];
                                                                   }
                                                   ];
                                              }
                              ];
                         }
         ];
    }
}


#pragma mark -
#pragma mark Settings

-(IBAction)settingsPushed{
    [sound_ValvePressed play];
    
    if(audio_on){
        [self toggleAudio];
    }
    
    
    [UIView animateWithDuration:0.1
                     animations:^{
                         settingsButton.center = CGPointMake(settingsButton.center.x, settingsButton.center.y+5);
                     }
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:0.1
                                               delay:nil
                                             options:UIViewAnimationOptionCurveEaseOut
                                          animations:^{
                                              settingsButton.center = CGPointMake(settingsButton.center.x, settingsButton.center.y-5);
                                          }
                                          completion:nil
                          ];
                     }
     ];
    
#ifdef LITEVERSION
    NSLog(@"showing lite settings");
    [self performSegueWithIdentifier:@"showLiteSettings" sender:nil];
#else
    NSLog(@"showing full settings");
    [self performSegueWithIdentifier:@"showFullSettings" sender:nil];
#endif
    
}

-(void)settingsDidFinish{
    #ifdef LITEVERSION      
    NSLog(@"Ad Timer: START");
    adTimer = [NSTimer scheduledTimerWithTimeInterval:adViewInterval target:self selector:@selector(showAdView) userInfo:nil repeats:NO];
    #endif
}


#pragma mark -
#pragma mark AdView control

#ifdef LITEVERSION
-(void)showAdView{
    if(audio_on){
        [self toggleAudio];
    }
    
    [self performSegueWithIdentifier:@"ShowAdSegue" sender:nil];
    
    NSLog(@"Ad Timer: INVALIDATE");
    [adTimer invalidate];
    
}


-(void)adViewDidFinish{
    NSLog(@"Ad Timer: START");
    adTimer = [NSTimer scheduledTimerWithTimeInterval:adViewInterval target:self selector:@selector(showAdView) userInfo:nil repeats:NO];
}

#endif


#pragma mark -
#pragma mark Segues

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    [segue.destinationViewController setDelegate:self]; 
    
#ifdef LITEVERSION    
    NSLog(@"Ad Timer: INVALIDATE");
    [adTimer invalidate];
#endif

}


#pragma mark -
#pragma mark Orientation

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return ( interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
