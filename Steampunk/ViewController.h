//
//  ViewController.h
//  Steampunk
//
//  Created by Ariel on 14/03/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

#include "Stk.h"
#include "Chorus.h"
#include "mo_filter.h"
#include "SineWave.h"
#include "Brass.h"

#include "Echo.h"
#include "JCRev.h"

#import "mo_accel.h"

#import "TutorialViewController.h"
#import "AdViewController.h"
#import "SettingsViewController.h"
#import "AboutViewController.h"

using namespace stk;

struct AudioData{
    Chorus *chorus1;
    MoOnePole *firstPole;
    MoOnePole *secondPole;
    SineWave *sine1;
    Brass *brass;
    
    Echo *echo1;
    JCRev *reverb1;
    MoAccel *myAccel;
    
};

@interface ViewController : UIViewController<AdViewDelegate, SettingsViewDelegate, TutorialViewDelegate>{
    
    IBOutlet UIButton *topValve;
    IBOutlet UIButton *middleValve;
    IBOutlet UIButton *bottomValve;
        
    IBOutlet UIButton *topFuse;
    IBOutlet UIButton *bottomFuse;
    
    IBOutlet UIImageView *slider;
    IBOutlet UIImageView *onOffSwitch;
    
    IBOutlet UIImageView *glower;
    
    IBOutlet UIImageView *greenGem;

    NSTimer *glowTimer;
    
    struct AudioData audioData;
    
    IBOutlet UIButton *settingsButton;
    
    TutorialViewController *tutorialView;
    AboutViewController *aboutView;
    
}

-(IBAction)topValvePressed;
-(IBAction)middleValvePressed;
-(IBAction)bottomValvePressed;

-(IBAction)toggleAudio;
-(IBAction)settingsPushed;

@property (nonatomic) IBOutlet UIButton *topValve;
@property (nonatomic) IBOutlet UIButton *middleValve;
@property (nonatomic) IBOutlet UIButton *bottomValve;

@property (nonatomic) IBOutlet UIButton *topFuse;
@property (nonatomic) IBOutlet UIButton *bottomFuse;

@property (nonatomic) IBOutlet UIImageView *slider;
@property (nonatomic) IBOutlet UIImageView *glower;
@property (nonatomic) IBOutlet UIImageView *greenGem;

@property (nonatomic) NSTimer *glowTimer;
@property (nonatomic) IBOutlet UIImageView *onOffSwitch;

@property (nonatomic) IBOutlet UIButton *settingsButton;

@property (nonatomic) TutorialViewController *tutorialView;




@end


