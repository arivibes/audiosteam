//
//  SplashViewController.m
//  Steampunk
//
//  Created by Ariel on 19/04/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SplashViewController.h"

@interface SplashViewController ()

@end

@implementation SplashViewController
@synthesize background, logo, instructions;

bool userEntered = false;

NSTimer *instructionsTimer;
UIButton *skipButton;
UIImageView *headphoneWarning;

CGFloat rotation;

AVAudioPlayer *sound_knobTurned;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)handleRotationGesture:(UIGestureRecognizer *) sender{
    rotation = [(UIRotationGestureRecognizer *) sender rotation];
    if(rotation>0 && !userEntered){
//        CGFloat velocity = [(UIRotationGestureRecognizer *) sender velocity];
        logo.transform = CGAffineTransformMakeRotation(rotation);
//        NSLog(@"Rotation: %f, velocity: %f", rotation, velocity);
        if(rotation > 1.54){
            [self performSelector:@selector(enter) withObject:nil afterDelay:1];
            
        }
    }
    if(sender.state == UIGestureRecognizerStateEnded){
        if(rotation < 1.54){
            [UIView animateWithDuration:1 animations:^{
                logo.transform = CGAffineTransformMakeRotation(rotation * -0.01);
                rotation = 0;
            }];
        }
    }
}

-(void)enter{
    if(!userEntered){
        [self performSegueWithIdentifier:@"showMain" sender:self];
        userEntered = true;
        [sound_knobTurned play];
        [instructionsTimer invalidate];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

#ifdef LITEVERSION
    NSLog(@"Starting AudioSteam LITE");
    NSLog(@" ");
#else
    NSLog(@"Starting AudioSteam FULL");
    NSLog(@" ");
#endif
    
    skipButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [skipButton setFrame:CGRectMake(0, 0, [[UIScreen mainScreen] applicationFrame].size.width , [[UIScreen mainScreen] applicationFrame].size.height)];
    [skipButton addTarget:self action:@selector(skipScreen) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:skipButton];
    
    instructions = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"splash_instruct.png"]];
    instructions.alpha = 0;
    [self.view insertSubview:instructions belowSubview:skipButton];
    
    
    UIImageView *esotericLogo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"esoteric-logo.png"]];
    [self.view insertSubview:esotericLogo belowSubview:skipButton];
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         esotericLogo.alpha = 0;
                         background.alpha = 1;
                     }
                     completion:^(BOOL finished){
                         if(finished){
                             [self toggleHeadphoneWarning];
                         }
                     }
     ];
                                              
    NSError *error = nil;
    sound_knobTurned = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:
                                                                    [NSString stringWithFormat:@"%@/knobTurned.caf", [[NSBundle mainBundle] resourcePath]]] error:&error];
    

    

}

-(void)skipScreen{
    if(headphoneWarning.alpha == 1){
        skipButton.userInteractionEnabled = NO;
        [UIView animateWithDuration:0.5
                         animations:^{
                             headphoneWarning.alpha = 0;
                         }
                         completion:^(BOOL finished){
                             [UIView animateWithDuration:0.2
                                              animations:^{
                                                  logo.alpha = 1;
                                              }
                              ];
                             instructionsTimer = [NSTimer scheduledTimerWithTimeInterval:4 target:self selector:@selector(showInstructions) userInfo:nil repeats:YES];
                         }
         ];

    }
}




-(void)toggleHeadphoneWarning{
    headphoneWarning = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"headphones_warning.png"]];
    [self.view insertSubview:headphoneWarning belowSubview:skipButton];
    headphoneWarning.alpha = 0;
    
    [UIView animateWithDuration:2
                     animations:^{headphoneWarning.alpha = 1;}
     ];
}

     
-(void)showInstructions{    
    if(rotation<0.2){
        [UIView animateWithDuration:2 animations:^{
            instructions.alpha = 1;
        }completion:^(BOOL finished){
            [UIView animateWithDuration:2 animations:^{
                instructions.alpha = 0;
            }];
        }];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
