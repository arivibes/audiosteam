//
//  AboutViewController.m
//  Steampunk
//
//  Created by Ariel Elkin on 12/07/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AboutViewController.h"

@implementation AboutViewController

-(IBAction)closeAboutView{
    NSLog(@"closing about view");
    
    [self dismissViewControllerAnimated:YES completion:nil];

}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    textView.font = [UIFont fontWithName:@"BaseTwelveSansBI" size:textView.font.pointSize];
    [textView setTextColor:[UIColor colorWithRed:0.11 green:0.44 blue:0.66 alpha:1]];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
