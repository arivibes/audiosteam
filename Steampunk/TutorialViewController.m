//
//  TutorialViewController.m
//  Steampunk
//
//  Created by Ariel on 02/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TutorialViewController.h"

@interface TutorialViewController ()

@end

@implementation TutorialViewController
@synthesize previousButton, nextButton, tutorialScreen;
@synthesize readInstructionsButton,jumpInButton;
@synthesize tutorialViewDelegate;
@synthesize screenNumber;

AVAudioPlayer *sound_PageTurns;
AVAudioPlayer *soundButtonPressed;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSError *error = nil;
    sound_PageTurns = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:
                                                              [NSString stringWithFormat:@"%@/tutorial_back_next.caf", [[NSBundle mainBundle] resourcePath]]] error:&error];

    self.view.alpha = 0;
    [self.view setBackgroundColor:[UIColor clearColor]];
    
    soundButtonPressed = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:
                                                                       [NSString stringWithFormat:@"%@/valvepress.caf", [[NSBundle mainBundle] resourcePath]]] error:&error];
    if(error != nil) NSLog(@"Error playing sound: %@", error);
    
    

}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [UIView animateWithDuration:0.3
                     animations:^{
                         nextButton.alpha = 1;
                     }
     ];
    
    
    previousButton.alpha = 0;
    if(screenNumber == 0){
        nextButton.alpha = 0;
    } else {
        nextButton.alpha = 1;
    }
    
}


-(IBAction)buttonTapped:(id)sender{
    
    
    if(sender == jumpInButton){
        NSLog(@"jump in tapped");
        [soundButtonPressed play];
        [self closeTutorial];
    }
    
    else if(sender == readInstructionsButton) {
        NSLog(@"read instructions tapped");
        
        [soundButtonPressed play];
        screenNumber++;
    }
    
    else if(sender == previousButton){
        [sound_PageTurns play];
        screenNumber --;
        if(screenNumber == 1){
            
            jumpInPressed.alpha = 0;
            readInstructionsPressed.alpha = 0;
            
            [UIView animateWithDuration:0.3
                             animations:^{
                                 previousButton.alpha = 0;
                             }
             ];
        }
    } 
    else if(sender == nextButton){
        [sound_PageTurns play];
        screenNumber++;
    }
        
    NSLog(@"Screennumber is %d", screenNumber);
    
    UIImage *nextImage = [[UIImage alloc] init];
    
    
    if(screenNumber == 1){
        nextImage = [UIImage imageNamed:@"tutorial_page1.png"];
        [UIView animateWithDuration:0.3
                         animations:^{
                             previousButton.alpha = 0;
                             nextButton.alpha = 1;
                             closeTutorialButton.alpha = 1;
                         }
         ];
    }
    
    else if(screenNumber == 2){
        nextImage = [UIImage imageNamed:@"tutorial_page2.png"];
        [UIView animateWithDuration:0.3
                         animations:^{previousButton.alpha = 1;}
         ];
    }
    
    else if(screenNumber == 3){
        nextImage = [UIImage imageNamed:@"tutorial_page3.png"];
    }
    
    else if(screenNumber == 4){
        nextImage = [UIImage imageNamed:@"tutorial_page4.png"];
        [UIView animateWithDuration:0.3
                         animations:^{nextButton.alpha = 1;}
         ];
    }
    
    else if(screenNumber == 5){
        nextImage = [UIImage imageNamed:@"tutorial_page5.png"];
        
        [UIView animateWithDuration:0.3
                         animations:^{nextButton.alpha = 0;}
         ];
        
    }
    [UIView animateWithDuration:0.5
                     animations:^{
                         tutorialScreen.alpha = 0;
                     }
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:0.5
                                          animations:^{
                                              tutorialScreen.image = nextImage;
                                              tutorialScreen.alpha = 1;
                                          }];
                     }
     ];

    if(screenNumber == 7){
        [self closeTutorial];
    }
    else if(sender == jumpInButton){
        
        [UIView animateWithDuration:0.1
                         animations:^{
                             jumpInPressed.alpha = 1;
                         }
                         completion:^(BOOL finished){
                             [self closeTutorial];
                         }
         ];
    }
}

-(IBAction)closeTutorial{
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.view.alpha = 0;
                     }
                     completion:^(BOOL finished){
                         if (finished) {
                             [self.view removeFromSuperview];
                             [tutorialViewDelegate tutorialViewClosed];
                         }
                     }
     ];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
