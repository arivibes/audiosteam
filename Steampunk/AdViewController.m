//
//  AdViewController.m
//  Steampunk
//
//  Created by Ariel Elkin on 09/07/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AdViewController.h"
#import <AVFoundation/AVFoundation.h>

@implementation AdViewController
@synthesize delegate;

NSTimer *countDownTimer; 
int currentCountDown;
AVAudioPlayer *laughSound;

-(void)bannerViewDidLoadAd:(ADBannerView *)banner{
    NSLog(@"AD VIEW: banner view did load ad!");
    adBanner.alpha = 1;
    [loadingImage removeFromSuperview];
}

-(void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error{
    adBanner.alpha = 0;
    [loadingImage setImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/ad_fail.png", [[NSBundle mainBundle] resourcePath]]]];
    NSLog(@"AD VIEW: banner view failed, error: %@", error);
}

- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave{
    [countDownTimer invalidate];
    return YES;
}

-(void)bannerViewWillLoadAd:(ADBannerView *)banner{
    NSLog(@"AD VIEW: banner view will load ad!");
    [loadingImage setImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/ad_loading.png", [[NSBundle mainBundle] resourcePath]]]];
}

+(void)bannerViewActionDidFinish:(ADBannerView *)banner{
    NSLog(@"AD VIEW: banner action finished");
    currentCountDown = 5;
    countDownTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countDown) userInfo:nil repeats:YES];
}


-(IBAction)getFullVersion{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms://itunes.com/apps/audiosteam"]];
}

-(void)countDown{
    currentCountDown--;
    countDownLabel.text = [NSString stringWithFormat:@"%d", currentCountDown];
    if (currentCountDown == 0) {
        [countDownTimer invalidate];
        [delegate adViewDidFinish];
        [self dismissModalViewControllerAnimated:YES];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [laughSound play];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    currentCountDown = 10;
    countDownLabel.text = [NSString stringWithFormat:@"%d", currentCountDown];
    countDownTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countDown) userInfo:nil repeats:YES];
    
    [UIView animateWithDuration:0.4
                          delay:0
                        options:UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse
                     animations:^{
                         loadingImage.alpha = 0.5;
                     }
                     completion:nil
     ];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    adBanner.delegate = self;
    NSLog(@"loaded ad banner view");
    
//    NSLog(@"Available Font Families: %@", [UIFont familyNames]);
    [countDownLabel setFont:[UIFont fontWithName:@"Kingthings Pique'n'meex" size:countDownLabel.font.pointSize]];
    
    NSError *error = nil; 
    laughSound = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:
                                                             [NSString stringWithFormat:@"%@/laugh.caf", [[NSBundle mainBundle] resourcePath]]] error:&error];
    


}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
