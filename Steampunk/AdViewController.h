//
//  AdViewController.h
//  Steampunk
//
//  Created by Ariel Elkin on 09/07/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>

@protocol AdViewDelegate <NSObject>

@optional
-(void)adViewDidFinish;

@end

@interface AdViewController : UIViewController <ADBannerViewDelegate>{
    IBOutlet ADBannerView *adBanner;
    IBOutlet UILabel *countDownLabel; 
    
    IBOutlet UIImageView *loadingImage;
    id <AdViewDelegate> delegate;
    
}

@property (strong) id <AdViewDelegate> delegate;

-(IBAction)getFullVersion;

@end
