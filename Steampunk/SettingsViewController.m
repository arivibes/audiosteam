//
//  SettingsViewController.m
//  Steampunk
//
//  Created by Ariel Elkin on 09/07/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SettingsViewController.h"


@implementation SettingsViewController
@synthesize delegate;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(IBAction)closeSettings{

    [self dismissViewControllerAnimated:YES completion:^{
        [delegate settingsDidFinish];}
     ];
}

-(IBAction)showTutorial{
    
    [self dismissViewControllerAnimated:YES completion:^{
        [delegate displayTutorial];
    }];
}

-(IBAction)showStory{
    [self dismissViewControllerAnimated:YES completion:^{
        [delegate displayStoryWithTutorial:NO];
    }];
}

#ifdef LITEVERSION
-(IBAction)getFullVersion{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms://itunes.com/apps/audiosteam"]];
}
#endif


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
