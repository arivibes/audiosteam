//
//  SettingsViewController.h
//  Steampunk
//
//  Created by Ariel Elkin on 09/07/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SettingsViewDelegate <NSObject>

-(void)settingsDidFinish;
-(void)displayTutorial;
-(void)displayStoryWithTutorial:(BOOL)showTutorial;

@end

@interface SettingsViewController : UIViewController{
    id <SettingsViewDelegate> delegate;
}

@property (strong) id <SettingsViewDelegate> delegate;

-(IBAction)closeSettings;
-(IBAction)showTutorial;


#ifdef LITEVERSION
-(IBAction)getFullVersion;
#endif

@end
