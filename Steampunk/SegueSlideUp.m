//
//  SegueFlipToInstrument.m
//  Steampunk
//
//  Created by Ariel Elkin on 29/07/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SegueSlideUp.h"

@implementation SegueSlideUp

-(void)perform{
    
    UIViewController *splashScreen = self.sourceViewController;
    UIViewController *mainScreen = self.destinationViewController;
    
    [splashScreen.view addSubview:mainScreen.view];
    
    mainScreen.view.center = CGPointMake(mainScreen.view.center.x, mainScreen.view.center.y-600);
    
    [UIView animateWithDuration:1
                     animations:^{
                         mainScreen.view.center = CGPointMake(mainScreen.view.center.x, [[UIScreen mainScreen] bounds].size.height/2);
                     }
                     completion:^(BOOL finished){
                         [splashScreen presentModalViewController:mainScreen animated:NO];
                     }
     ];
}

@end
