//
//  TutorialViewController.h
//  Steampunk
//
//  Created by Ariel on 02/05/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@protocol TutorialViewDelegate <NSObject>

-(void)tutorialViewClosed;

@end

@interface TutorialViewController : UIViewController{

    IBOutlet UIButton *previousButton;
    IBOutlet UIButton *nextButton;
    IBOutlet UIImageView *tutorialScreen;
    
    IBOutlet UIButton *readInstructionsButton;
    IBOutlet UIButton *jumpInButton;
    
    IBOutlet UIImageView *jumpInPressed;
    IBOutlet UIImageView *readInstructionsPressed;
    
    IBOutlet UIButton *closeTutorialButton;
    
    int screenNumber;

}

-(IBAction)buttonTapped:(id)sender;
-(IBAction)closeTutorial;

@property (nonatomic, strong) IBOutlet UIButton *previousButton;
@property (nonatomic, strong) IBOutlet UIButton *nextButton;
@property (nonatomic, strong) IBOutlet UIImageView *tutorialScreen;

@property (nonatomic, strong) IBOutlet UIButton *readInstructionsButton;
@property (nonatomic, strong) IBOutlet UIButton *jumpInButton;

@property int screenNumber;

@property (strong) id <TutorialViewDelegate> tutorialViewDelegate;

@end
