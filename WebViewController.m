//
//  WebViewController.m
//  Steampunk
//
//  Created by Ariel Elkin on 09/07/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "WebViewController.h"

@implementation WebViewController

-(void)webViewDidStartLoad:(UIWebView *)webView{
    [activityIndicator startAnimating];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [activityIndicator stopAnimating];
}

-(void)webView:(UIWebView *)someWebView didFailLoadWithError:(NSError *)error{
    
    NSLog(@"loading failed!");
    [activityIndicator stopAnimating];
    
    UIImageView *loadingErrorImage = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/web_no_connection.png", [[NSBundle mainBundle] resourcePath]]]];
    loadingErrorImage.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2+30);
    [self.view addSubview:loadingErrorImage];
    [self.view insertSubview:loadingErrorImage aboveSubview:webView];
    
    
    
//    if (error != NULL && [error code] != -999) {
//        [activityIndicator stopAnimating];
//        UIAlertView *errorAlert = [[UIAlertView alloc]
//                                   initWithTitle: [error localizedDescription]
//                                   message: [error localizedFailureReason]
//                                   delegate:nil
//                                   cancelButtonTitle:@"OK" 
//                                   otherButtonTitles:nil];
//        [errorAlert show];
//    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    
    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background.png"]];
    background.center = CGPointMake(160, 240);
    [self.view addSubview:background];
    [self.view sendSubviewToBack:background];
    webView.scalesPageToFit = YES;
    
	// Do any additional setup after loading the view.
}

-(IBAction)closeWebView{
    [self dismissModalViewControllerAnimated:YES];
}

-(IBAction)showFacebook{
    [webView loadRequest:[NSURLRequest requestWithURL:
						  [NSURL URLWithString:@"http://m.facebook.com/AudioSteam?v=wall"]
						  ]
	 ];
}

-(IBAction)showTwitter{
    [webView loadRequest:[NSURLRequest requestWithURL:
						  [NSURL URLWithString:@"https://mobile.twitter.com/audiosteam"]
						  ]
	 ];
}

-(IBAction)showWebsite{
    [webView loadRequest:[NSURLRequest requestWithURL:
						  [NSURL URLWithString:@"http://www.marazita.com/audiosteam"]
						  ]
	 ];
}

-(void)viewDidAppear:(BOOL)animated{
    webView.dataDetectorTypes = UIDataDetectorTypeAll;
	
	[webView loadRequest:[NSURLRequest requestWithURL:
						  [NSURL URLWithString:@"http://m.facebook.com/AudioSteam?v=wall"]
						  ]
	 ];

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
